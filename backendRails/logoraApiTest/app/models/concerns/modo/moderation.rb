require 'httparty'
# Le concern de modération
module Moderable
  extend ActiveSupport::Concern
  class_methods do
    def moderation(*columns)
      define_method(:moderate_after) do
        if columns.any? { |column| saved_change_to_attribute?(column) }
          content = columns.map { |column| send(column) }.join('')
          response = Moderable.send_to_moderation(content)
          update(is_accepted: response)
        end
      end

      after_save :moderate_after
    end
  end

  def self.send_to_moderation(content)
    response = HTTParty.get(
      'https://moderation.logora.fr/predict',
      body: { content: content }.to_json,
      headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
    )
    JSON.parse(response.body)
  end
end
