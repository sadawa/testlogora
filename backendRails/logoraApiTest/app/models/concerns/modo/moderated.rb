require './moderation'
# Une classe qui souhaite faire appel à la modération de ses attributs
class ModeratedModel < ApplicationRecord
  include Moderable
  moderation(columns: [:text, :language])
end
