import express from "express";
import axios from "axios";
import colors from "colors";
import dotenv from "dotenv";

//Configuration ENV
dotenv.config();

//Middleware
const app = express();
app.use(express.json());

//routes
app.post("/api/moderation/predict", async (req, res) => {
  const { text, language } = req.body;

  try {
    const response = await axios.get("https://moderation.logora.fr/predict", {
      params: {
        text: text,
        language: language,
      },
    });

    res.json(response.data);
  } catch (error) {
    console.log("Error", error);
    res.status(500).json({
      message: "Erreur lors de l'appel à l'API externe",
      error: error.message,
    });
  }
});

app.post("/api/moderation/score", async (req, res) => {
  const { text, language } = req.body;

  try {
    const response = await axios.get("https://moderation.logora.fr/score", {
      params: {
        text: text,
        language: language,
      },
    });

    res.json(response.data);
  } catch (error) {
    console.log("Error", error);
    res.status(500).json({
      message: "Erreur lors de l'appel à l'API externe",
      error: error.message,
    });
  }
});

const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(`Server runnig in ${PORT} mode on port ${PORT}`.yellow.bold)
);
export { app };
