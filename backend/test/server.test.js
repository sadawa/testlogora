import request from "supertest";
import { it, describe, expect } from "vitest";
import { app } from "../server.js";

//Test de la route de POST /api/moderation/predict
describe("POST /api/moderation/predict", () => {
  it("should return a prediction response", async () => {
    const response = await request(app)
      .post("/api/moderation/predict")
      .send({ text: "Example texte", language: "fr-FR" })
      .expect("Content-Type", /json/)
      .expect(200);

    expect(response.body).toHaveProperty("prediction");
  });

  it("should default to French language if none is specified", async () => {
    const response = await request(app)
      .post("/api/moderation/predict")
      .send({ text: "Un exemple de texte" })
      .expect("Content-Type", /json/)
      .expect(200);

    expect(response.body).toHaveProperty("prediction");
  });
});
//Test de la route de POST /api/moderation/score
describe("POST /api/moderation/score", () => {
  it("should return a score response", async () => {
    const response = await request(app)
      .post("/api/moderation/predict")
      .send({ text: "Example text score", language: "en-EN" })
      .expect("Content-Type", /json/)
      .expect(200);

    expect(response.body).toHaveProperty("prediction");
  });
});
